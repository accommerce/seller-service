const express = require('express')
const cors = require('cors')
const session = require('express-session')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const passport = require('passport')
const { sendError, CustomError } = require('accommerce-helpers')

const indexRouter = require('./routes/index')
const sellersRouter = require('./routes/sellers')
const shopsRouter = require('./routes/shops')

const app = express()

app.use(cors())

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(session({ secret: Math.random().toString(), resave: false, saveUninitialized: false }))

app.use(passport.initialize())
app.use(passport.session())

app.use('/', indexRouter)
app.use('/api/sellers', sellersRouter)
app.use('/api/shops', shopsRouter)

// catch 404 and forward to error handler
app.use('*', (req, res, next) => {
    const error = CustomError(`URL không tồn tại!`, `URL does not exist!`, 404)
    next(error)
})

// error handler
app.use(sendError)

module.exports = app
