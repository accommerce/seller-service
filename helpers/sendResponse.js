module.exports =
    (data, message = 'OK', status = 200) =>
    (req, res, next) => {
        res.status(status).json({ success: true, data, message })
    }
