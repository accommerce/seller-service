const Bluebird = require('bluebird')
const { Sizes, Products, Connection } = require('../models/models')
const { CustomError } = require('accommerce-helpers')

exports.createNewProduct = async (args) => {
    const session = await Connection.startSession()
    try {
        session.startTransaction()

        const { sizes, ...rest } = args

        const product = await Products.create([rest], { session })

        const sizesWithProductId = sizes.map((size) => ({ ...size, product: product[0]._id }))

        await Sizes.create(sizesWithProductId, { session })

        const [createdProduct, createdSizes] = await Bluebird.all([
            Products.findById(product[0]._id).lean().session(session),
            Sizes.find({ product: product[0]._id }).lean().session(session),
        ])

        const result = { ...createdProduct, sizes: createdSizes }

        await session.commitTransaction()

        return result
    } catch (error) {
        session.abortTransaction()
        throw error
    } finally {
        session.endSession()
    }
}

exports.editProduct = async (id, args) => {
    const session = await Connection.startSession()
    try {
        session.startTransaction()

        const product = await Products.findById(id).lean()

        const { sizes, ...rest } = args

        const { price, originalPrice } = { ...product, ...rest }
        if (price > originalPrice) {
            throw CustomError(
                `Giá bán không thể lớn hơn giá gốc!`,
                `Selling price cannot be greater than original price!`,
                400
            )
        }

        const updatedProduct = await Products.findByIdAndUpdate(
            id,
            { $set: rest },
            { new: true, session }
        )
            .lean()
            .exec()

        const updatedSizes = sizes
            ? await Bluebird.map(
                  sizes,
                  (size) => {
                      const { name } = size

                      return Sizes.findOneAndUpdate(
                          { name, product: id },
                          { $set: { ...size, product: id } },
                          { upsert: true, new: true, session }
                      )
                          .lean()
                          .exec()
                  },
                  { concurrency: sizes.length }
              )
            : await Sizes.find({ product: id }).lean().session(session)

        await Sizes.deleteMany({
            _id: { $nin: updatedSizes.map((size) => size._id) },
            product: id,
        }).session(session)
        

        await session.commitTransaction()

        return { ...updatedProduct, sizes: updatedSizes }
    } catch (error) {
        session.abortTransaction()
        throw error
    } finally {
        session.endSession()
    }
}

exports.deleteProducts = async (shopId, productIds) => {
        const session = await Connection.startSession()
        try {
            session.startTransaction()

            const productsToDelete = await Products.find({
                _id: { $in: productIds },
                shop: shopId,
            })
                .select('_id')
                .lean()

            if (!productsToDelete.length) {
                throw CustomError(
                    `Không tìm thấy sản phẩm cần xóa`,
                    `Product to delete not found`,
                    404
                )
            }

            const idsToDelete = productsToDelete.map((product) => product._id)

            await Products.updateMany(
                {
                    _id: { $in: idsToDelete },
                },
                { $set: { deletedAt: new Date() } },
                { session }
            )

            await Sizes.deleteMany({ product: { $in: idsToDelete } }).session(session)
            

            await session.commitTransaction()

            return productsToDelete.length
        } catch (error) {
            session.abortTransaction()
            throw error
        } finally {
            session.endSession()
        }
}

exports.createNewProductSize = async (args) => {
    const { product, name } = args

    return Sizes.findOneAndUpdate(
        { product, name },
        { $set: args },
        { upsert: true, new: true }
    ).lean()
}

exports.deleteProductSizes = async (productId, sizeIds) => {
    return Sizes.deleteMany({ _id: { $in: sizeIds }, product: productId })
}
