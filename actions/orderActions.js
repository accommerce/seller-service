const Bluebird = require('bluebird')
const { Orders, OrderItems, Users, Products, Sizes, Connection } = require('../models/models')
const { ORDER_STATUSES } = require('accommerce-helpers')

exports.searchOrders = async ({ shopId, status }) => {
    const query = { shop: shopId }

    status && (query.status = status)

    const orders = await Orders.find(query)
        .populate({
            path: 'customer',
            model: Users,
            select: 'firstName lastName phoneNumber',
        })
        .sort('-_id')
        .lean()

    return Bluebird.map(
        orders,
        async (order) => {
            const orderItems = await OrderItems.find({ order: order._id })
                .populate({
                    path: 'product',
                    model: Products,
                })
                .lean()

            const totalPrice = orderItems.reduce(
                (total, item) => total + item.quantity * item.price,
                0
            )

            return { ...order, totalPrice, items: orderItems }
        },
        { concurrency: orders.length }
    )
}

const _updateSizesWhenCancelOrder = async (orderId, session) => {
    const orderItems = await OrderItems.find({ order: orderId }).lean()

    return Bluebird.map(
        orderItems,
        async (item) => {
            const { product, size, quantity } = item

            await Sizes.updateOne(
                { product, name: size },
                { $inc: { numberInStock: quantity } },
                { session }
            )
        },
        { concurrency: orderItems.length }
    )
}

exports.cancelOrder = async (orderId) => {
    const session = await Connection.startSession()
    try {
        session.startTransaction()

        const [cancelledOrder] = await Bluebird.all([
            Orders.findByIdAndUpdate(
                orderId,
                { $set: { status: ORDER_STATUSES.CANCELLED_BY_SELLER } },
                { new: true, session }
            ).lean(),
            _updateSizesWhenCancelOrder(orderId, session),
        ])

        await session.commitTransaction()

        return cancelledOrder
    } catch (error) {
        await session.abortTransaction()
        throw error
    } finally {
        await session.endSession()
    }
}

exports.confirmOrder = async (orderId) => {
    return Orders.findByIdAndUpdate(
        orderId,
        { $set: { status: ORDER_STATUSES.IN_TRANSIT } },
        { new: true }
    ).lean()
}
