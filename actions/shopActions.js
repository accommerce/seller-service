const { Shops, Users } = require('../models/models')

exports.createNewShop = async (args) => {
    const newShop = await Shops.create(args)

    return Shops.findById(newShop._id).lean()
}

exports.getAllShops = async (sellerId, isActive = true, approvalStatus = 'approved') => {
    const shops = Shops.find({
        seller: sellerId,
        deletedAt: null,
        isActive: isActive !== 'false',
        approvalStatus,
    }).lean()

    return shops
}
