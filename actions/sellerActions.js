const { Users } = require('../models/models')

exports.registerSeller = async (userId) => {
    const user = await Users.findById(userId)

    !user.roles.includes('seller') && user.roles.push('seller')

    return user.save()
}
