const Joi = require('joi')
const { Shops } = require('../models/models')
const { CustomError } = require('accommerce-helpers')

const shopSchema = Joi.object({
    name: Joi.string()
        .trim()
        .max(255)
        .required()
        .error(
            CustomError(
                `Tên cửa hàng là bắt buộc và có tối đa 255 ký tự!`,
                `Shop name is required and must less than or equals to 255 characters!`
            )
        ),
    email: Joi.string()
        .email()
        .required()
        .error(CustomError(`Email không hợp lệ!`, `Email invalid!`)),
    address: Joi.string()
        .trim()
        .max(255)
        .required()
        .error(
            CustomError(
                `Địa chỉ cửa hàng là bắt buộc và có tối đa 255 ký tự!`,
                `Shop address is required and must less than or equals to 255 characters!`
            )
        ),
})

exports.validateNewShop = async (req, res, next) => {
    const { error, value } = shopSchema.validate(req.body)

    if (error) {
        error.status = 400
        return next(error)
    }

    req.body = { ...value, seller: req.user._id }
    next()
}

exports.validateShopOwner = async (req, res, next) => {
    try {
        const { shopId } = req.params
        const shop = await Shops.findById(shopId).lean()

        if (!shop) {
            throw CustomError(
                `Cửa hàng ${shopId} không tồn tại!`,
                `Shop ${shopId} does not exist!`,
                404
            )
        }

        if (shop.seller.toString() !== req.user._id.toString()) {
            throw CustomError(`Bạn không phải chủ shop!`, `You are not shop owner!`, 403)
        }

        if (shop.approvalStatus !== 'approved') {
            throw CustomError(
                `Cửa hàng chưa được duyệt yêu cầu mở từ Admin!`,
                `Only shop with approval status 'approved' is allowed to do this action. Current status: '${shop.approvalStatus}'`,
                403
            )
        }

        next()
    } catch (error) {
        next(error)
    }
}