const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const { CustomError } = require('accommerce-helpers')

const { Products } = require('../models/models')

const sizeSchema = Joi.object({
    name: Joi.string()
        .trim()
        .max(255)
        .required()
        .error(
            CustomError(
                `Tên size là bắt buộc và có độ dài tối đa 255 ký tự!`,
                `Product size is required and less than or equals 255 characters!`
            )
        ),
    numberInStock: Joi.number()
        .min(1)
        .max(9999)
        .required()
        .error(
            CustomError(
                `Số sản phẩm trong kho phải nằm trong khoảng từ 1 đến 9999!`,
                `Number in stock must be between 1 and 9999!`
            )
        ),
})

const editSizeSchema = Joi.object({
    name: Joi.string()
        .trim()
        .max(255)
        .required()
        .error(
            CustomError(
                `Tên size là bắt buộc và có độ dài tối đa 255 ký tự!`,
                `Product size is required and less than or equals 255 characters!`
            )
        ),
    numberInStock: Joi.number()
        .min(0)
        .max(9999)
        .required()
        .error(
            CustomError(
                `Số sản phẩm trong kho phải nằm trong khoảng từ 0 đến 9999!`,
                `Number in stock must be between 1 and 9999!`
            )
        ),
})

const newProductSchema = Joi.object({
    name: Joi.string()
        .trim()
        .max(255)
        .required()
        .error(
            CustomError(
                `Tên sản phẩm là bắt buộc và có độ dài tối đa 255 ký tự!`,
                `Product name is required and less than or equals to 255 characters!`
            )
        ),
    description: Joi.string()
        .trim()
        .required()
        .error(CustomError(`Mô tả sản phẩm là bắt buộc!`, `Product description ís required!`)),
    category: Joi.string().trim(),
    sizes: Joi.array()
        .items(sizeSchema)
        .min(1)
        .unique((a, b) => a.name === b.name)
        .required(),
    images: Joi.array()
        .items(Joi.string().trim())
        .min(1)
        .required()
        .error(CustomError(`Sản phẩm cần tối thiểu 1 ảnh!`, `Product needs at least 1 image!`)),
    price: Joi.number()
        .positive()
        .precision(2)
        .required()
        .error(CustomError(`Giá bán sản phẩm không hợp lệ!`, `Product selling price invalid!`)),
    originalPrice: Joi.number()
        .positive()
        .precision(2)
        .required()
        .error(CustomError(`Giá gốc sản phẩm không hợp lệ!`, `Product original price invalid!`)),
})

exports.validateNewProduct = async (req, res, next) => {
    try {
        const { shopId } = req.params

        const { error, value } = newProductSchema.validate(req.body)

        if (error) {
            error.status = 400
            return next(error)
        }

        const { price, originalPrice } = value
        if (price > originalPrice) {
            throw CustomError(
                `Giá bán không thể lớn hơn giá gốc!`,
                `Selling price cannot be greater than original price!`,
                400
            )
        }

        req.body = { ...value, shop: shopId }
        next()
    } catch (error) {
        next(error)
    }
}

exports.validateProductShop = async (req, res, next) => {
    try {
        const { shopId, productId } = req.params

        const product = await Products.findById(productId).lean()

        if (!product) {
            throw CustomError(
                `Sản phẩm ${productId} không tồn tại!`,
                `Product ${productId} not found!`,
                404
            )
        }

        if (product.shop.toString() !== shopId.toString()) {
            throw CustomError(
                `Cửa hàng ${shopId} không chứa sản phẩm ${productId}`,
                `Shop ${shopId} does not has product ${productId}`,
                400
            )
        }

        next()
    } catch (error) {
        next(error)
    }
}

const editProductSchema = Joi.object({
    name: Joi.string()
        .trim()
        .max(255)
        .error(
            CustomError(
                `Tên sản phẩm có độ dài tối đa 255 ký tự!`,
                `Product name must less than or equals to 255 characters!`
            )
        ),
    description: Joi.string().trim(),
    category: Joi.string().trim(),
    sizes: Joi.array()
        .items(editSizeSchema)
        .min(1)
        .unique((a, b) => a.name === b.name),
    images: Joi.array()
        .items(Joi.string().trim())
        .min(1)
        .error(CustomError(`Sản phẩm cần tối thiểu 1 ảnh!`, `Product needs at least 1 image!`)),
    price: Joi.number()
        .positive()
        .precision(2)
        .error(CustomError(`Giá bán sản phẩm không hợp lệ!`, `Product selling price invalid!`)),
    originalPrice: Joi.number()
        .positive()
        .precision(2)
        .error(CustomError(`Giá gốc sản phẩm không hợp lệ!`, `Product original price invalid!`)),
})

exports.validateEditProduct = async (req, res, next) => {
    try {
        const { error, value } = editProductSchema.validate(req.body)

        if (error) {
            error.status = 400
            return next(error)
        }

        const { price, originalPrice } = value
        if (price > originalPrice) {
            throw CustomError(
                `Giá bán không thể lớn hơn giá gốc!`,
                `Selling price cannot be greater than original price!`,
                400
            )
        }

        req.body = value
        next()
    } catch (error) {
        next(error)
    }
}

exports.validateNewProductSize = async (req, res, next) => {
    try {
        const { value: validatedSize, error } = sizeSchema.validate(req.body)

        if (error) {
            error.status = 400
            return next(error)
        }

        const { productId } = req.params

        validatedSize.product = productId
        req.body = validatedSize
        next()
    } catch (error) {
        next(error)
    }
}

const deleteProductSizesSchema = Joi.object({
    sizeIds: Joi.array().items(Joi.objectId()).min(1).required(),
})

exports.validateDeleteProductSizes = async (req, res, next) => {
    try {
        const { value, error } = deleteProductSizesSchema.validate(req.body)

        if (error) {
            error.status = 400
            return next(error)
        }

        next()
    } catch (error) {
        next(error)
    }
}