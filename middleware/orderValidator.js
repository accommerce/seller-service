const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const { Orders } = require('../models/models')
const { ORDER_STATUSES, ORDER_STATUSES_MAPPING, CustomError } = require('accommerce-helpers')

exports.validateChangeOrderStatus = async (req, res, next) => {
    try {
        const { orderId } = req.params

        const { error } = Joi.objectId().validate(orderId)

        if (error) {
            throw new Error(error)
        }

        const order = await Orders.findById(orderId).lean()

        if (!order) {
            throw CustomError(`Đơn hàng không tồn tại`, `Order not found`, 404)
        }

        if (order.status !== ORDER_STATUSES.WAITING_FOR_SELLER_CONFIRM) {
            throw CustomError(
                `Bạn chỉ có thể xác nhận hoặc hủy đơn hàng đang ở trạng thái ${
                    ORDER_STATUSES_MAPPING[ORDER_STATUSES.WAITING_FOR_SELLER_CONFIRM]
                }. Trạng thái hiện tại: ${ORDER_STATUSES_MAPPING[order.status]}`,
                `Can only change status of order has status is ${ORDER_STATUSES.WAITING_FOR_SELLER_CONFIRM}. Current status: ${order.status}`,
                403
            )
        }

        next()
    } catch (error) {
        next(error)
    }
}
