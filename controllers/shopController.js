const { sendResponse } = require('accommerce-helpers')
const { createNewShop, getAllShops } = require('../actions/shopActions')

exports.createNewShop = async (req, res, next) => {
    try {
        const args = req.body

        const shop = await createNewShop(args)

        sendResponse(
            shop,
            'Đăng ký mở của hàng thành công. Vui lòng chờ Admin xác nhận yêu cầu',
            'Request to open the store successfully. Please wait for the admin to approve the request',
            201
        )(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.getAllShops = async (req, res, next) => {
    try {
        const sellerId = req.user._id
        const { isActive, approvalStatus } = req.query

        const shops = await getAllShops(sellerId, isActive, approvalStatus)

        sendResponse(shops)(req, res, next)
    } catch (error) {
        next(error)
    }
}
