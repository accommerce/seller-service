const { sendResponse } = require('accommerce-helpers')
const { registerSeller } = require('../actions/sellerActions')

exports.registerSeller = async (req, res, next) => {
    try {
        const _id = req.user._id

        const seller = await registerSeller(_id)

        sendResponse(
            seller,
            'Đăng ký trở thành người bán thành công',
            'Request to become seller successfully'
        )(req, res, next)
    } catch (error) {
        next(error)
    }
}
