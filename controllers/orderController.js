const { sendResponse, CustomError } = require('accommerce-helpers')
const { searchOrders, cancelOrder, confirmOrder } = require('../actions/orderActions')

exports.searchOrders = async (req, res, next) => {
    try {
        const { status } = req.query
        const { shopId } = req.params

        const orders = await searchOrders({ shopId, status })

        sendResponse(orders)(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.confirmOrder = async (req, res, next) => {
    try {
        const { orderId } = req.params

        const order = await confirmOrder(orderId)

        sendResponse(order)(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.cancelOrder = async (req, res, next) => {
    try {
        const { orderId } = req.params

        const order = await cancelOrder(orderId)

        sendResponse(order)(req, res, next)
    } catch (error) {
        next(error)
    }
}
