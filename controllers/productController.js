const { sendResponse, CustomError } = require('accommerce-helpers')
const {
    createNewProduct,
    editProduct,
    deleteProducts,
    createNewProductSize,
    deleteProductSizes,
} = require('../actions/productActions.js')

exports.createNewProduct = async (req, res, next) => {
    try {
        const args = req.body

        const product = await createNewProduct(args)

        sendResponse(
            product,
            'Tạo sản phẩm mới thành công',
            'Product created successfully!',
            201
        )(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.deleteProducts = async (req, res, next) => {
    try {
        const { productIds } = req.body
        const { shopId } = req.params

        const deletedCount = await deleteProducts(shopId, productIds)

        sendResponse(
            true,
            `Xóa thành công ${deletedCount} sản phẩm`,
            `Deleted ${deletedCount} product(s) successfully`
        )(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.editProduct = async (req, res, next) => {
    try {
        const args = req.body
        const { productId } = req.params

        const product = await editProduct(productId, args)

        sendResponse(
            product,
            'Cập nhật sản phẩm thành công',
            'Product updated successfully',
            200
        )(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.createNewProductSize = async (req, res, next) => {
    try {
        const args = req.body

        const size = await createNewProductSize(args)

        sendResponse(
            size,
            'Thêm size mới thành công',
            'Created new size successfully',
            201
        )(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.deleteProductSizes = async (req, res, next) => {
    try {
        const { sizeIds } = req.body
        const { productId } = req.params

        const response = await deleteProductSizes(productId, sizeIds)

        sendResponse(
            response,
            `Xóa thành công ${response.deletedCount} sản phẩm`,
            `Deleted ${response.deletedCount} size(s) successfully`
        )(req, res, next)
    } catch (error) {
        next(error)
    }
}
