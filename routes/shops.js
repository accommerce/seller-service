const express = require('express')
const router = express.Router()
const shopController = require('../controllers/shopController')
const productsRouter = require('../routes/products')
const ordersRouter = require('../routes/orders')
const { verifyLogin, verifyRole } = require('../auth/auth')
const { validateNewShop, validateShopOwner } = require('../middleware/shopValidator')

/**
 * All APIs below need authorized as seller
 */
router.use(verifyRole('seller'))

/**
 * CREATE new shop
 */
router.post('/', validateNewShop, shopController.createNewShop)
router.get('/', shopController.getAllShops)

router.use('/:shopId', validateShopOwner)

/**
 * Products Router
 */
router.use('/:shopId/products', productsRouter)

/**
 * Orders router
 */
router.use('/:shopId/orders', ordersRouter)

module.exports = router
