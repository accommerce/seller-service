const express = require('express')
const router = express.Router()
const sellerController = require('../controllers/sellerController')
const { verifyLogin, verifyRole } = require('../auth/auth')

/**
 * All APIs below need authentication
 */
router.use(verifyLogin)

router.post('/', sellerController.registerSeller)

module.exports = router
