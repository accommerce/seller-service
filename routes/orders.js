const express = require('express')
const router = express.Router({ mergeParams: true })
const { verifyRole } = require('../auth/auth')
const orderController = require('../controllers/orderController')
const { validateChangeOrderStatus } = require('../middleware/orderValidator')

router.get('/', orderController.searchOrders)
router.put('/:orderId/status/confirm', validateChangeOrderStatus, orderController.confirmOrder)
router.put('/:orderId/status/cancel', validateChangeOrderStatus, orderController.cancelOrder)

module.exports = router
