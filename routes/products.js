const express = require('express')
const router = express.Router({ mergeParams: true })
const productController = require('../controllers/productController.js')
const {
    validateProductShop,
    validateNewProduct,
    validateEditProduct,
    validateNewProductSize,
    validateDeleteProductSizes,
} = require('../middleware/productValidator')

router.post('/', validateNewProduct, productController.createNewProduct)
router.delete('/', productController.deleteProducts)

router.use('/:productId', validateProductShop)

router.put('/:productId', validateEditProduct, productController.editProduct)
router.post('/:productId/sizes', validateNewProductSize, productController.createNewProductSize)
router.delete('/:productId/sizes', validateDeleteProductSizes, productController.deleteProductSizes)

module.exports = router
